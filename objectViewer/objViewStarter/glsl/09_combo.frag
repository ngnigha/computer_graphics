
uniform vec4 lma; // Light-Material ambient
uniform vec4 lmd; // Light-Material diffuse
uniform vec4 lms; // Light-Material specular
uniform float gloss;

uniform sampler2D normalMap;
uniform sampler2D decal;
uniform sampler2D heightField;
uniform samplerCube envmap;

uniform mat3 objectToWorld;

varying vec2 normalMapTexCoord;
varying vec3 lightDirection;
varying vec3 eyeDirection;
varying vec3 halfAngle;
varying vec3 c0, c1, c2;

void main()
{
  vec3 T = normalize(c0);
  vec3 N = normalize(c2);
  vec3 B = normalize( cross( T, N ) );
  vec3 sN= vec3(0,0,1);

  mat3 M = mat3( vec3(T.x, B.x, N.x), vec3(T.y, B.y, N.y), vec3(T.z, B.z, N.z) );
  vec3 L = normalize(lightDirection * M);
  vec3 E = normalize(eyeDirection * M);
  vec3 H = normalize(halfAngle * M);

  // Bumping
  vec3 bump = 2.0 * vec3( texture2D(normalMap, normalMapTexCoord*vec2(1.0, 1.0)) ) - 1.0;
  bump = normalize(M * bump);

/*
  // Reflection
  vec3 R = reflect(E, bump);
  R = normalize(R);
  vec4 rTerm = textureCube( envmap, R);

  // decal
  vec4 dCal = texture2D(decal, normalMapTexCoord);  // XXX fix me
*/

  // Ambient
  vec4 ambient = lma;

  // Adding bump diffuse
  float kd = max( 0.0, dot(bump, L ));
  vec4 diffuse = kd * lmd ;
  diffuse = clamp(diffuse, 0.0, 1.0);

/*
  // Specular
  float ks = kd != 0.0 ? pow( max(dot(sN, H), 0.0), gloss) : 0.0;
  vec4 specular = lms * ks;
  specular = clamp(specular, 0.0, 1.0);
*/

  gl_FragColor = ambient + diffuse;  // XXX fix me

}