
uniform vec4 lma; // Light-Material ambient
uniform vec4 lmd; // Light-Material diffuse
uniform vec4 lms; // Light-Material specular
uniform float gloss;

uniform sampler2D normalMap;
uniform sampler2D decal;
uniform sampler2D heightField;
uniform samplerCube envmap;

uniform mat3 objectToWorld;

varying vec2 normalMapTexCoord;
varying vec3 lightDirection;
varying vec3 eyeDirection;
varying vec3 halfAngle;
varying vec3 c0, c1, c2;

void main()
{
  vec3 T = normalize(c0);
  vec3 N = normalize(c2);
  vec3 B = normalize( cross(N, T) );
  vec3 sN= vec3(0,0,1);

  mat3 M = mat3( vec3(T.x, B.x, N.x), vec3(T.y, B.y, N.y), vec3(T.z, B.z, N.z) );
  vec3 L = normalize(M * lightDirection);
  vec3 H = normalize(M * halfAngle);

  vec4 ambient = lma;

  float kd = max( dot(L, sN), 0.0 );
  vec4 diffuse = lmd * kd;
  diffuse = clamp(diffuse, 0.0, 1.0);

  float ks = kd != 0.0 ? pow( max(dot(sN, H), 0.0), gloss) : 0.0;
  vec4 specular = lms * ks;
  specular = clamp(specular, 0.0, 1.0);

  gl_FragColor = specular;  // XXX fix me
}
