
uniform vec4 lma; // Light-Material ambient
uniform vec4 lmd; // Light-Material diffuse
uniform vec4 lms; // Light-Material specular
uniform float gloss;

uniform sampler2D normalMap;
uniform sampler2D decal;
uniform sampler2D heightField;
uniform samplerCube envmap;

uniform mat3 objectToWorld;

varying vec2 normalMapTexCoord;
varying vec3 lightDirection;
varying vec3 eyeDirection;
varying vec3 halfAngle;
varying vec3 c0, c1, c2;

void main()
{
  vec3 T = normalize(c0);
  vec3 N = normalize(c2);
  vec3 B = normalize( cross( N, T ) );
  vec3 sN= vec3(0,0,1);

  mat3 M = mat3( vec3(T.x, B.x, N.x), vec3(T.y, B.y, N.y), vec3(T.z, B.z, N.z) );
  vec3 L = normalize(M * lightDirection);
  vec3 H = normalize(M * halfAngle);

  vec3 bump = 2.0 * vec3( texture2D(normalMap, normalMapTexCoord*vec2(1.0, 1.0)) ) - 1.0;  // XXX fix me
  bump = normalize(bump);

  float kd = max( 0.0, dot(bump, L ));
  vec4 diffuse = kd * lmd ;
  diffuse = clamp(diffuse, 0.0, 1.0);

  gl_FragColor = lma + diffuse;
}
