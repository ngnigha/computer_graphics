
attribute vec2 parametric;

uniform vec3 lightPosition;  // Object-space
uniform vec3 eyePosition;    // Object-space
uniform vec2 torus_info;

varying vec2 normalMapTexCoord;

varying vec3 lightDirection;
varying vec3 halfAngle;
varying vec3 eyeDirection;
varying vec3 c0, c1, c2;



void main()
{

  float pi = radians(360);
  float R = torus_info.x;
  float r = torus_info.y;
  float u = parametric.x;
  float v = parametric.y;

  normalMapTexCoord = vec2( v, u );  // XXX fix me
  vec4 vx = vec4( (R + r*cos(u*pi))*cos(v*pi), (R + r*cos(u*pi))*sin(v*pi), r*sin(u*pi), 1);

  gl_Position = gl_ModelViewProjectionMatrix * vx;  // XXX fix me

  eyeDirection = vec3(vx) - eyePosition;  // XXX fix me
  lightDirection = vec3(vx) - lightPosition;  // XXX fix me
  halfAngle = (eyeDirection + lightDirection) / 2.0;  // XXX fix me

  // Gradients
  c0 = vec3( -pi*r*sin(pi*u)*cos(pi*v), -pi*r*sin(pi*u)*sin(pi*v), r*pi*cos(pi*u));  // XXX fix me
  c1 = vec3( -pi*sin(pi*v)*(R + r*cos(pi*u)), pi*cos(pi*v)*(R + r*cos(pi*u)), 0);  // XXX fix me
  c2 = cross( normalize(c0), normalize(c1) );  // XXX fix me
}

