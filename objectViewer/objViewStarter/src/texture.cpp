#include "texture.hpp"
#include "imageload/stb_image.h"
#include <iostream>
using namespace std;
extern const char *program_name;
extern bool verbose;

TextureImage::TextureImage()
    : filename(NULL)
    , width(0)
    , height(0)
    , components(0)
    , orig_components(0)
    , image(NULL)
{}

TextureImage::TextureImage(const char *fn)
    : width(0)
    , height(0)
    , components(0)
    , orig_components(0)
    , image(NULL)
{
    filename = new char[strlen(fn)+1];
    assert(filename);
    strcpy(filename, fn);
}

TextureImage::~TextureImage()
{
    stbi_image_free(image);
    delete filename;
}

bool TextureImage::load()
{
    components = 4;
    return loadImage();
}

bool TextureImage::loadImage()
{
    image = stbi_load(filename, &width, &height, &orig_components, components);
    if (image) {
        string fn(filename);
        if (fn.rfind(".jpg") != string::npos
            || fn.rfind(".jpeg") != string::npos
            || fn.rfind(".JPG") != string::npos
            || fn.rfind(".JPEG") != string::npos)
        {
            verticalFlip();
        }
        return true;
    } else {
        printf("%s: failed to load image %s\n", program_name, filename);
        return false;
    }
}

void TextureImage::verticalFlip()
{
    int stride = width * components;
    int top = 0;
    int bottom = stride * (height - 1);
    unsigned char buffer;
    while (top < bottom) {
        for (int i = 0; i < stride; i++) {
            unsigned char* target1 = image + top + i;
            unsigned char* target2 = image + bottom + i;
            buffer = *target1;
            *target1 = *target2;
            *target2 = buffer;
        }
        top += stride;
        bottom -= stride;
    }
}

/******************************************************************************/

TextureGLState::TextureGLState()
    : mipmapped(true)
    , texture_object(0)
{
}

TextureGLState::~TextureGLState()
{
    if (texture_object) {
        glDeleteTextures(1, &texture_object);
    }
}

void TextureGLState::tellGL()
{
    if (texture_object == 0) {
        glGenTextures(1, &texture_object);
    }
}

GLuint TextureGLState::getTextureObject()
{
    return texture_object;
}

bool TextureGLState::isMipmapped()
{
    return mipmapped;
}

/******************************************************************************/
Texture::Texture(const char *fn)
    : TextureImage(fn)
{
}

Texture::~Texture()
{
}

/******************************************************************************/

Texture2D::Texture2D(const char *fn)
    : Texture(fn)
{
}

Texture2D::~Texture2D()
{
}

void Texture2D::bind()
{
    glBindTexture(target, getTextureObject());
}

void Texture2D::bind(GLenum texture_unit)
{
    glActiveTexture(texture_unit);
    glBindTexture(target, getTextureObject());
}

void Texture2D::tellGL()
{
    TextureGLState::tellGL();

    bind();
    glTexImage2D(target, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    if (isMipmapped()) {
        glGenerateMipmap(target);
        glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    } else {
        glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
}

/******************************************************************************/

NormalMap::NormalMap(const char *fn)
    : Texture2D(fn)
    , normal_image(NULL)
{}

struct PackedNormal {
    GLubyte n[3];

    PackedNormal(vec3 normal);
};

PackedNormal::PackedNormal(vec3 normal)
{
    assert(normal.x >= -1);
    assert(normal.y >= -1);
    assert(normal.z >= -1);

    assert(normal.x <= 1);
    assert(normal.y <= 1);
    assert(normal.z <= 1);
    n[0] = 128 + 127 * normal.x;
    n[1] = 128 + 127 * normal.y;
    n[2] = 128 + 127 * normal.z;
}

float luminance( const GLubyte rgb[3] )
{
    return (.2126f*rgb[0] + .7152f*rgb[1] + .0722*rgb[2]) / 256.0;
}

vec3 NormalMap::computeNormal(int i, int j, float scale)
{
    GLubyte *A = image + ( (width * j)) + ((i-1)%width)  * 3;
    GLubyte *B = image + ( (width * j)) + ((i+1)%width)  * 3;
    GLubyte *C = image + ( (width * ((j-1)%height)) + i ) * 3;
    GLubyte *D = image + ( (width * ((j+1)%height)) + i ) * 3;

    float x = luminance(A) - luminance(B);
    float y = luminance(C) - luminance(D);
    float z = sqrt( 1 - x*x - y*y);

    cout << "xyz: " << x << " " << y << " " << z << endl;
    vec3 normal = vec3(x, y, z) * scale;
    // normal = normal * vec3(0.5, 0.5, 1.0) + vec3(0.5, 0.5, 0.0);
    normal = clamp(normal, vec3{-1.0f, -1.0f, -1.0f}, vec3{1.0f, 1.0f, 1.0f});
    cout << normal.x << " E " << normal.y << " ^ " << normal.z <<" scale " <<  scale<< endl;
    return normal;
}

bool NormalMap::load(float scale)
{
    components = 1;
    if(loadImage()) {
        assert(width > 0);
        assert(height > 0);
        assert(orig_components > 0);
        if (orig_components != components) {
            if (verbose) {
                printf("warning: %d component normal map treated as gray scale height map\n",
                    orig_components);
            }
        }
        normal_image = new GLubyte[width * height * 3];
        assert(normal_image);
        GLubyte* p = normal_image;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                // cout << (uint)p[0] << " =i " << (uint)p[1] << " " << (uint)p[2] << endl;
                vec3 normal = computeNormal(x, y, scale);
                PackedNormal packed_normal(normal);
                p[0] = packed_normal.n[0];
                p[1] = packed_normal.n[1];
                p[2] = packed_normal.n[2];
                // cout << (uint)p[0] << " =O " << (uint)p[1] << " " << (uint)p[2] << endl;
                p += 3;
            }
        }
        assert(p == normal_image + (width * height * 3));
        // success
        return true;
    } else {
        printf("%s: failed to load image %s\n", program_name, filename);
        return false;
    }
}

void NormalMap::tellGL()
{
    TextureGLState::tellGL();

    bind();
    glTexImage2D(target, 0, GL_RGB8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, normal_image);
    if (isMipmapped()) {
        glGenerateMipmap(target);
        glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    } else {
        glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
}

NormalMap::~NormalMap()
{
    delete normal_image;
}

/******************************************************************************/

static const char *face_name[6] = {
    "xpos",
    "xneg",
    "ypos",
    "yneg",
    "zpos",
    "zneg"
};

#if defined(_MSC_VER) && _MSC_VER < 1900

#define snprintf c99_snprintf
#define vsnprintf c99_vsnprintf

__inline int c99_vsnprintf(char *outBuf, size_t size, const char *format, va_list ap)
{
    int count = -1;

    if (size != 0)
        count = _vsnprintf_s(outBuf, size, _TRUNCATE, format, ap);
    if (count == -1)
        count = _vscprintf(format, ap);

    return count;
}

__inline int c99_snprintf(char *outBuf, size_t size, const char *format, ...)
{
    int count;
    va_list ap;

    va_start(ap, format);
    count = c99_vsnprintf(outBuf, size, format, ap);
    va_end(ap);

    return count;
}

#endif

CubeMap::CubeMap(const char *filename_pattern)
{
    for (int i = 0; i < 6; i++) {
        char buffer[200];

        snprintf(buffer, sizeof(buffer), filename_pattern, face_name[i]);
        face[i].filename = new char[strlen(buffer)+1];
        strcpy(face[i].filename, buffer);
    }
}

CubeMap::~CubeMap()
{
}

bool CubeMap::load()
{
    bool success = true;
    for (int i = 0; i < 6; i++) {
        success &= face[i].load();
    }
    return success;
}

void CubeMap::tellGL()
{
    TextureGLState::tellGL();

    bind();

    GLint base_level = 0;
    for (int i = 0; i < 6; i++) {
        TextureImage &img = face[i];

        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X+i, base_level,
            GL_RGBA8, img.width, img.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img.image);
    }
    if (isMipmapped()) {
        glGenerateMipmap(target);
        glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    } else {
        glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
    glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

void CubeMap::bind()
{
    glBindTexture(target, getTextureObject());
}

void CubeMap::bind(GLenum texture_unit)
{
    glActiveTexture(texture_unit);
    glBindTexture(target, getTextureObject());
}

static const GLfloat vertex[4*6][3] = {
    /* Positive X face. */
    { 1, -1, -1 },  { 1, 1, -1 },  { 1, 1, 1 },  { 1, -1, 1 },
    /* Negative X face. */
    { -1, -1, -1 },  { -1, 1, -1 },  { -1, 1, 1 },  { -1, -1, 1 },
    /* Positive Y face. */
    { -1, 1, -1 },  { 1, 1, -1 },  { 1, 1, 1 },  { -1, 1, 1 },
    /* Negative Y face. */
    { -1, -1, -1 },  { 1, -1, -1 },  { 1, -1, 1 },  { -1, -1, 1 },
    /* Positive Z face. */
    { -1, -1, 1 },  { 1, -1, 1 },  { 1, 1, 1 },  { -1, 1, 1 },
    /* Negative Z face. */
    { -1, -1, -1 },  { 1, -1, -1 },  { 1, 1, -1 },  { -1, 1, -1 },
};

void CubeMap::draw(float scale)
{
    GLenum texture_unit = GL_TEXTURE3;
    glUseProgram(0);

    bind(texture_unit);
    glActiveTexture(texture_unit);
    glEnable(GL_TEXTURE_CUBE_MAP);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glScalef(scale, scale, scale);
    glBegin(GL_QUADS);
        /* For each vertex of each face of the cube... */
        for (int i = 0; i < 4*6; i++) {
            glMultiTexCoord3fv(texture_unit, vertex[i]);
            glVertex3f(vertex[i][0], vertex[i][1], vertex[i][2]);
        }
    glEnd();
    glPopMatrix();
    glDisable(GL_TEXTURE_CUBE_MAP);
    glActiveTexture(GL_TEXTURE0);
}
