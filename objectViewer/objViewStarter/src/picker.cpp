#include "picker.hpp"
#include "scene.hpp"

#include <iostream>
using namespace std;

/******************************************************************************/

Picker::Picker(ScenePtr s) : scene(s)
{
    reset();
}

void Picker::reset()
{
    /****** Initialize (or reset to initial state) ******/
    cout << "Rst--" << endl;
    objptr = nullptr;
    cur_t = 0;
    cur_pt = vec4{0,0,0,0};
    pickRay = RayPtr(new Ray(vec4(0.0f, 0.0f, 0.0f, 1.0f), vec4(0.0f, 0.0f, 1.0f, 0.0f)));
}

void Picker::setScene(ScenePtr s)
{
    cout << "scene**" << endl;
    scene = s;
    reset();
}

RayPtr Picker::mouseRay(int x, int y)
{
    /****** Make a world coordinate ray from mouse x and y coords ******/
    RayPtr curRay = RayPtr(new Ray(vec4(0.0f, 0.0f, 0.0f, 1.0f), vec4(0.0f, 0.0f, 1.0f, 0.0f)));

    // GLclampd zNear, zFar;
    GLint viewportMat[4];

    glGetIntegerv(GL_VIEWPORT,viewportMat);
    // Getting the normalized screen space
    GLfloat xClip =  ((x - viewportMat[0]) * 2.0 / viewportMat[2]) - 1;
    GLfloat yClip = -((y - viewportMat[1]) * 2.0 / viewportMat[3]) + 1;
    GLfloat zClip = -1.0;
    GLfloat wClip =  1.0;

    // Get the project matrix
    vec4 ray_clip{xClip, yClip, zClip, wClip};
    vec4 ray_eye = inverse(scene->viewer->camera->getProjectionMatrix()) * ray_clip;

    ray_eye = vec4(ray_eye.x, ray_eye.y, -1.0, 1.0);

    curRay->origin = vec4(scene->viewer->eye_position, 1);
    curRay->direction = ray_eye;
    pickRay = curRay;
    pickRay->transform( inverse(scene->viewer->getViewMatrix()) );


    GLint viewport[4]; //var to hold the viewport info
    GLdouble modelview[16]; //var to hold the modelview info
    GLdouble projection[16]; //var to hold the projection matrix info
    GLfloat winX, winY, winZ; //variables to hold screen x,y,z coordinates
    GLdouble worldX, worldY, worldZ; //variables to hold world x,y,z coordinates
    GLfloat z; //  = BufferUtils.createBuffer(1);
    glReadPixels(x,y,1,1,GL_DEPTH_COMPONENT, GL_FLOAT, &z);

    glGetDoublev( GL_MODELVIEW_MATRIX, modelview ); //get the modelview info
    glGetDoublev( GL_PROJECTION_MATRIX, projection ); //get the projection matrix info
    glGetIntegerv( GL_VIEWPORT, viewport ); //get the viewport info

    winX = (float)x;
    winY = (float)viewport[3] - (float)y;
    winZ = z;;

    //get the world coordinates from the screen coordinates
    gluUnProject( winX, winY, winZ, modelview, projection, viewport, &worldX, &worldY, &worldZ);
    cout << worldX << " **** " << worldY << " z = " << worldZ << endl;
    pickRay->direction = vec4{worldX, worldY, worldZ, 1};
    pickRay->direction = normalize(pickRay->direction);
    return pickRay;
}

void Picker::makeList(RayPtr ray)
{
    /****** Make a list of objects hit by the ray ******/
    cout << "MakeList " << scene->object_list.size() << endl;
    for( ObjectPtr obj : scene->object_list )
    {
        float t = ray->intersect( obj->aabb );
        if( t > 0 )
        {
            HitPtr hPtr;
            hPtr->t = t;
            hPtr->pt= ray->point(t);
            hPtr->o = obj;
            hitlist.push_back(hPtr);
            if( !curObj() || (t < cur_t) )
            {
                objptr = hPtr->o;
                cur_pt = hPtr->pt;
                cur_t  = hPtr->t;
            }
        }
    }
}

bool Picker::nextObj()
{
    cout << "Nextobj" << endl;
    /****** Get the next ojbect in the list ******/
    return false;
}


bool Picker::curObj()
{
    cout << "curObj" << endl;
    /****** Check to see if there's a current object ******/
    return (objptr != nullptr);
}
