#include "controller.hpp"
#include "scene.hpp"
#include "ray.hpp"

#include <iostream>
using namespace std;

#define EPSLN 0.0001

extern ScenePtr scene;
extern float window_heightf, window_widthf;

Controller::Controller() {}

Controller::~Controller() {}

ObjectController::ObjectController()
{
    mode = MOVE;
    moving = false;
    picker = PickerPtr(new Picker(scene));
	bump_height = 0.0f;
	new_spin_update = true;
	animate_object_spinning = true;
	moving = false;
	spinning = false;
}

ObjectController::ObjectController(ScenePtr s) : scene(s)
{
    mode = MOVE;
    moving = false;
    picker = PickerPtr(new Picker(scene));
}

void ObjectController::setScene(ScenePtr s)
{
    scene = s;
    object = scene->object_list.back();
}

void ObjectController::keyboard(unsigned char key, int x, int y)
{
    switch (key) {
    case 'b':
        object = scene->object_list.back();
        bump_height += 0.1;
        printf("bump_height = %f\n", bump_height);

        object->material->normal_map->load(bump_height);
        cout <<"over"<<endl;
        object->material->normal_map->tellGL();

        object->material->bindTextures();
        break;
    case 'B':
        object = scene->object_list.back();
        bump_height -= 0.1;
        printf("bump_height = %f\n", bump_height);

        object->material->normal_map->load(bump_height);
        object->material->normal_map->tellGL();

        object->material->bindTextures();
        break;
    case 'c':
        // toggle animation
        animate_object_spinning = !animate_object_spinning;
        break;
    default:
        break;
    }
    glutPostRedisplay();
}

void ObjectController::mouse(int button, int state, int x, int y)
{
    int modifiers = glutGetModifiers();
    if (modifiers & GLUT_ACTIVE_CTRL) {
        if (button == GLUT_MIDDLE_BUTTON)
        {
            if (state == GLUT_DOWN)
            {
                cout << " SPIN SET " << endl;

                /****** Get selected object and set it to spin
                        1) Use picker to return the object pointer
                        2) Set appropriate control variables in the object
                        3) Manage object highlighting as needed ******/
                picker->reset();
                RayPtr ray = picker->mouseRay(x, y);
                picker->makeList(ray);
                object = picker->objptr;

                begin_spin_x = x;
                begin_spin_y = y;
                moving = false;
                spinning = true;
            }
            else if (state == GLUT_UP && spinning)
            {
                /****** Set object state and highlight controls ******/
                cout <<"SPINNING" << endl;
                picker->reset();
                RayPtr ray = picker->mouseRay(x, y);
                picker->makeList(ray);
                object = picker->objptr;

                begin_spin_x = x;
                begin_spin_y = y;
                spinning = false;
                moving = false;
            }
        }
        else if (button == GLUT_LEFT_BUTTON)
        {
            /****** Similar to above ******/
            cout << " ROTATION CTRL " << endl;
            picker->reset();
            RayPtr ray = picker->mouseRay(x, y);
            picker->makeList(ray);
            object = picker->objptr;

            begin_spin_x = x;
            begin_spin_y = y;
            moving = true;
            spinning = false;
        }
    }
    if (modifiers & GLUT_ACTIVE_SHIFT) {
        if (button == GLUT_LEFT_BUTTON) {
            if (state == GLUT_DOWN)
            {
                /****** Similar to above ******/
                cout <<" ROTATION SHIFT " << endl;
                picker->reset();
                RayPtr ray = picker->mouseRay(x, y);
                picker->makeList(ray);
                object = picker->objptr;

                begin_spin_x = x;
                begin_spin_y = y;
                spinning = true;
                moving = false;
            }
        }
    }
}

void ObjectController::motion(int x, int y)
{
    if (spinning)
    {
      /****** Compute quaternion from the input
              mouse x and y and the stored old values of x and y, then
              apply it to update the rotation either here, once, or in the
              idle callback if animation mode is on ******/
        cout << " Spinning " << endl;
    }
    if (moving)
    {
        cout << " Moving " << endl;
        // float alpha = radians( step * (x - delx) );
        // float beta  = radians( step * (y - dely) );

        // mat4 transMat{ vec4{1,0,0,0}, vec4{0,1,0,0}, vec4{0,0,1,0}, vec4{lookAt.x-eye.x, lookAt.y-eye.y, lookAt.z-eye.z, 1} };
        // mat4 transRev{ vec4{1,0,0,0}, vec4{0,1,0,0}, vec4{0,0,1,0}, vec4{-lookAt.x+eye.x, -lookAt.y+eye.y, -lookAt.z+eye.z, 1} };
        // mat4 rotXaMat{ vec4{1,0,0,0}, vec4{0, cos(beta), sin(beta), 0}, vec4{0, -sin(beta), cos(beta),0}, vec4{0,0,0,1} };
        /****** Compute translation, no need to animate ******/
    }
}

void ObjectController::stopSpinning()
{
    object->do_idle = false;
}

void ObjectController::doTransform()
{
  /****** Do actual transformation for either rotation or translation ******/
    glutPostRedisplay();
}

/******************************************************************************/

PickController::PickController()
{
    picker = PickerPtr(new Picker());
}


PickController::PickController(ScenePtr s) : scene(s)
{
    picker = PickerPtr(new Picker(scene));
}

void PickController::setScene(ScenePtr s)
{
    scene = s;
    picker->setScene(s);
}

void PickController::keyboard(unsigned char key, int x, int y)
{
  /* Pick modes, n = pick next object pointed to, p= pick mode */
    if (key == 'n') {
        if (picker->curObj()) picker->objptr->highlight(false);
        if (picker->nextObj()) picker->objptr->highlight(true);
    }
    if (key == 'p') {
        if (picker->curObj()) picker->objptr->highlight(false);
        picker->reset();
    }
    glutPostRedisplay();
}

void PickController::mouse(int button, int state, int x, int y)
{
  cout << "MSE"<< endl;
  /* Handle left button press */
    int modifiers = glutGetModifiers();
    if (button == GLUT_LEFT_BUTTON)
        if (state == GLUT_DOWN)
            if (picker->curObj()) {
                scene->current_object = picker->objptr;
                if (!(modifiers & GLUT_ACTIVE_CTRL))
                        picker->objptr->highlight(false);
            }
}

void PickController::passivemotion(int x, int y)
{
  /* Handle motion without button press */
    if (picker->curObj()) picker->objptr->highlight(false);
    picker->makeList(picker->mouseRay(x, y));
    if (picker->curObj()) picker->objptr->highlight(true);
}

/******************************************************************************/

CameraController::CameraController(ViewerPtr view) : viewer(view) {}

void CameraController::setPosition()
{
    viewer->setPosition();
}

/******************************************************************************/

CylinderController::CylinderController(ViewerPtr view)
    : CameraController(view), eye_height(0), eye_angle(0)
{
    reset();
}

void CylinderController::reset()
{
    eye_radius = distance(viewer->eye_position, viewer->at_position);
    setPosition();
}


void CylinderController::keyboard(unsigned char c, int x, int y){}


void CylinderController::mouse(int button, int state, int x, int y)
{
    cout <<"Mouse"<<endl;
    if (button == GLUT_LEFT_BUTTON)
        if (state == GLUT_DOWN) {
            begin_x = x;
            begin_y = y;
        }
}

void CylinderController::motion(int x, int y)
{
  cout <<"Motion"<<endl;
    spinDegrees(-0.2 * (x - begin_x));
    lift(0.02 * (y - begin_y));
    setPosition();

    glutPostRedisplay();
    begin_x = x;
    begin_y = y;
}

void CylinderController::spinDegrees(float angle)
{
    eye_angle += radians(angle);
}

void CylinderController::lift(float height)
{
    eye_height += height;
}

void CylinderController::setPosition()
{
    vec2 on_circle_position = eye_radius*vec2(sin(eye_angle), cos(eye_angle));
    viewer->eye_position = viewer->at_position
        + vec3(on_circle_position.x, eye_height, on_circle_position.y);
    viewer->setPosition();
}

/******************************************************************************/

OrbStrafeController::OrbStrafeController(ViewerPtr view) : CameraController(view)
{
    reset();
}

void OrbStrafeController::reset()
{
    /******Initialize camera state******/
    cout <<"Reset"<<endl;
    view_scale = .1;
    step = .2;
    upVec = vec3{0,1,0};

    BBox box;
    if( !scene->object_list.empty() )
    {
        ObjectPtr obj = scene->object_list.back();
        box = obj->aabb;
    }

    lookAt = box.centroid;
    eye = viewer->eye_position;
    // eye = vec3{0,0,64};
    viewVec = lookAt - eye;
    delx = lookAt.x;
    dely = lookAt.y;
    radius = distance(viewer->eye_position, viewer->at_position);
    moving = false;

    setPosition();
}

void OrbStrafeController::keyboard(unsigned char key, int x, int y)
{
  /******Handle character key controls here ******/

  cout <<"key "<<key<<endl;
  switch (key) {
  case 'w': {
    special( GLUT_KEY_UP, x, y );
    break;
  }
  case 'a': {
    special( GLUT_KEY_LEFT, x, y );
    break;
  }
  case 's': {
    special( GLUT_KEY_DOWN, x, y );
    eye -= step * normalize(viewVec);
    break;
  }
  case 'd': {
    special( GLUT_KEY_RIGHT, x, y );
    break;
  }
  default:
    break;
  }
  setPosition();
}

void OrbStrafeController::special(int key, int x, int y)
{
  /******Handle special key controls here (e.g. arrow keys)******/
  switch (key) {
  case GLUT_KEY_UP: {
    vec3 tp = step * normalize(viewVec);
    if( length(lookAt - eye - tp) > 1e-3)
        eye += tp;
    break;
  }
  case GLUT_KEY_LEFT: {
    float alpha = radians( step * 2 );
    vec4 tp1{eye.x, eye.y, eye.z, 1};
    vec4 tp2{lookAt.x, lookAt.y, lookAt.z, 1};

    mat4 rotYaMat{ vec4{cos(alpha),0,-sin(alpha),0}, vec4{0,1,0,0}, vec4{sin(alpha),0,cos(alpha),0}, vec4{0,0,0,1} };
    mat4 rotYaRev = transpose(rotYaMat);

    eye = vec3( rotYaMat * tp1);
    lookAt = vec3( rotYaRev * tp2);
    break;
  }
  case GLUT_KEY_DOWN: {
    eye -= step * normalize(viewVec);
    break;
  }
  case GLUT_KEY_RIGHT: {
    float alpha = radians( step * -2 );
    vec4 tp1{eye.x, eye.y, eye.z, 1};
    vec4 tp2{lookAt.x, lookAt.y, lookAt.z, 1};

    mat4 rotYaMat{ vec4{cos(alpha),0,-sin(alpha),0}, vec4{0,1,0,0}, vec4{sin(alpha),0,cos(alpha),0}, vec4{0,0,0,1} };
    mat4 rotYaRev = transpose(rotYaMat);

    eye = vec3( rotYaMat * tp1);
    lookAt = vec3( rotYaRev * tp2);
    break;
    break;
  }
  default:
    break;
  }
  setPosition();
}

void OrbStrafeController::mouse(int button, int state, int x, int y)
{
    /******Handle mouse inputs here******/
    if (button == GLUT_LEFT_BUTTON)
        if (state == GLUT_DOWN) {
            delx = x;
            dely = y;
            moving  = true;
        }
}

void OrbStrafeController::motion(int x, int y)
{
    /* Orbital camera w/o using spherical coordinates */
    moving = (x != delx | x != dely);
    if(moving)
    {
        /******Move the camera******/
        float xx = step * (x - delx);
        float yy = step * (y - dely);
        float alpha = radians(xx);
        float beta  = radians(yy);
        mat4 rotXaMat{ vec4{1,0,0,0}, vec4{0, cos(beta), sin(beta), 0}, vec4{0, -sin(beta), cos(beta),0}, vec4{0,0,0,1} };
        mat4 rotYaMat{ vec4{cos(alpha),0,-sin(alpha),0}, vec4{0,1,0,0}, vec4{sin(alpha),0,cos(alpha),0}, vec4{0,0,0,1} };

        vec4 tp{eye.x, eye.y, eye.z, 1};
        eye = vec3(rotYaMat * rotXaMat * tp);
        cout <<eye.x << " " << eye.y << " " << eye.z << endl;
        setPosition();
        delx = x;
        dely = y;
        moving = false;
    }
}

void OrbStrafeController::setPosition()
{
    /******Set the camera position (may not be the same as Cylinder)******/
    viewer->eye_position = eye;
    viewer->at_position = lookAt;
    viewer->up_vector = upVec;
    viewer->setPosition();
    viewVec = lookAt - eye;
    glutPostRedisplay();
}

void OrbStrafeController::setSpeed(float s)
{
    /******Control the speed******/
    cout <<"Setting speed " << s <<endl;
    step = s;
}

/******************************************************************************/

VehicleController::VehicleController(ViewerPtr view) : CameraController(view)
{
    reset();
}

void VehicleController::reset()
{
  /******Initialize camera state******/
    cout <<"Reset"<<endl;
    view_scale = 1;
    step = .2;
    upVec = vec3{0,1,0};

    BBox box;
    if( !scene->object_list.empty() )
    {
        ObjectPtr obj = scene->object_list.back();
        box = obj->aabb;
    }

    lookAt = box.centroid;
    eye = viewer->eye_position;
    viewVec = lookAt - eye;
    delx = lookAt.x;
    dely = lookAt.y;
    float radius = distance(viewer->eye_position, viewer->at_position);
    moving = false;
    setPosition();
}

void VehicleController::keyboard(unsigned char key, int x, int y)
{
  /******Handle character key controls here ******/

  switch (key) {
  case 'w': {
    special( GLUT_KEY_UP, x, y );
    break;
  }
  case 'a': {
    special( GLUT_KEY_LEFT, x, y );
    break;
  }
  case 's': {
    special( GLUT_KEY_DOWN, x, y );
    eye -= step * normalize(viewVec);
    break;
  }
  case 'd': {
    special( GLUT_KEY_RIGHT, x, y );
    break;
  }
  default:
    break;
  }
  setPosition();
}

void VehicleController::special(int key, int x, int y)
{
  /******Handle special key controls here (e.g. arrow keys)******/
  switch (key) {
  case GLUT_KEY_UP: {
    vec3 tp = step * normalize(viewVec);
    if( length(lookAt - eye - tp) > 1e-3)
        eye += tp;
    break;
  }
  case GLUT_KEY_LEFT: {
    float alpha = radians( step * 2 );
    vec4 tp1{eye.x, eye.y, eye.z, 1};
    vec4 tp2{lookAt.x, lookAt.y, lookAt.z, 1};

    mat4 rotYaMat{ vec4{cos(alpha),0,-sin(alpha),0}, vec4{0,1,0,0}, vec4{sin(alpha),0,cos(alpha),0}, vec4{0,0,0,1} };
    mat4 rotYaRev = transpose(rotYaMat);

    eye = vec3( rotYaMat * tp1);
    lookAt = vec3( rotYaRev * tp2);
    break;
  }
  case GLUT_KEY_DOWN: {
    eye -= step * normalize(viewVec);
    break;
  }
  case GLUT_KEY_RIGHT: {
    float alpha = radians( step * -2 );
    vec4 tp1{eye.x, eye.y, eye.z, 1};
    vec4 tp2{lookAt.x, lookAt.y, lookAt.z, 1};

    mat4 rotYaMat{ vec4{cos(alpha),0,-sin(alpha),0}, vec4{0,1,0,0}, vec4{sin(alpha),0,cos(alpha),0}, vec4{0,0,0,1} };
    mat4 rotYaRev = transpose(rotYaMat);

    eye = vec3( rotYaMat * tp1);
    lookAt = vec3( rotYaRev * tp2);
    break;
    break;
  }
  default:
    break;
  }
  setPosition();
}

void VehicleController::mouse(int button, int state, int x, int y)
{
    /******Handle mouse inputs here******/
    if (button == GLUT_LEFT_BUTTON)
        if (state == GLUT_DOWN) {
            delx = x;
            dely = y;
            // moving = true;
        }
}

void VehicleController::motion(int x, int y)
{
    moving = (x != delx | x != dely);

    if(moving)
    {
        /******Move the camera******/
        float alpha = radians( step * (x - delx) );
        float beta  = radians( step * (y - dely) );

        mat4 transMat{ vec4{1,0,0,0}, vec4{0,1,0,0}, vec4{0,0,1,0}, vec4{lookAt.x-eye.x, lookAt.y-eye.y, lookAt.z-eye.z, 1} };
        mat4 transRev{ vec4{1,0,0,0}, vec4{0,1,0,0}, vec4{0,0,1,0}, vec4{-lookAt.x+eye.x, -lookAt.y+eye.y, -lookAt.z+eye.z, 1} };
        mat4 rotXaMat{ vec4{1,0,0,0}, vec4{0, cos(beta), sin(beta), 0}, vec4{0, -sin(beta), cos(beta),0}, vec4{0,0,0,1} };
        mat4 rotYaMat{ vec4{cos(alpha),0,-sin(alpha),0}, vec4{0,1,0,0}, vec4{sin(alpha),0,cos(alpha),0}, vec4{0,0,0,1} };

        vec4 tp{lookAt.x, lookAt.y, lookAt.z, 1};
        tp = transRev * rotXaMat * rotYaMat * transMat * tp;
        lookAt = vec3(tp);

        setPosition();
        delx = x;
        dely = y;
        // moving = false;
    }
}

void VehicleController::setPosition()
{
    /******Set the camera position (may not be the same as Cylinder)******/
    viewer->eye_position = eye;
    viewer->at_position = lookAt;
    viewer->up_vector = upVec;
    viewer->setPosition();
    viewVec = lookAt - eye;
    glutPostRedisplay();
}

void VehicleController::setSpeed(float s)
{
    /******Control the speed******/
    step = s;
}
