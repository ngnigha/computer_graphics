#include "ray.hpp"
#include <iostream>

using namespace std;

#define EPSILON 2.e-3

Ray::Ray(vec4 o, vec4 d) : origin(o), direction(d)
{
    direction = normalize(direction);
}

Ray& Ray::operator =(const Ray& rhs)
{
    if (this != &rhs) {
        origin = rhs.origin;
        direction = rhs.direction;
    }
    return *this;
}

void Ray::transform(mat4x4 m)
{
    /****** Tranform the ray (origin and direction vector) ******/
    // origin = m * origin;
    direction = (m * direction);
    direction.z = -direction.z;
    direction = normalize(direction);
}

float get_t( const PlanePtr p, const Ray *ray )
{
    float t = -1;
    float a1 = dot(p->n, vec3(ray->origin));
    float a2 = dot(p->n, vec3(ray->direction));

    if( abs(a2) <= EPSILON )
        return t;

    t  = -(a1 + p->d) / a2;
    return t;
}

float Ray::intersect(BBox aabb)
{
    float t = -1.0f;

    cout << "-O- " << origin.x << " * " << origin.y << " * " << origin.z << endl;
    cout << "--- " << aabb.lbn.x << " * " << aabb.lbn.y << " * " << aabb.lbn.z << endl;
    cout << "--- " << aabb.rtf.x << " * " << aabb.rtf.y << " * " << aabb.rtf.z << endl;

    /****** Return t of intersection with given bounding box ******/
    for(int i = 0; i < 6; i++)
    {
        PlanePtr p = aabb.planes[i];
        float res = get_t(p, this);
        if(res > 0)
        {
            vec4 pt = point(res);
            if(
                pt.x >= aabb.lbn.x - EPSILON && pt.x <= aabb.rtf.x + EPSILON &&
                pt.y >= aabb.lbn.y - EPSILON && pt.y <= aabb.rtf.y + EPSILON &&
                pt.z >= aabb.lbn.z - EPSILON && pt.z <= aabb.rtf.z + EPSILON &&
                true //(t > res || t < 0)
              )
            {
                t = res;
                cout << "\t\t" << pt.x << " x* " << pt.y << " x* " << pt.z << " " << pt.w << endl;
            }
        }
    }

    return -1;
}

vec4 Ray::point(float t)
{
    /****** Return point at t along ray ******/
    vec4 pt = origin + t * direction;
    // pt = pt / pt.w;
    cout << "\t" << pt.x << " x " << pt.y << " x " << pt.z << " " << pt.w << endl;
    return pt;
}
