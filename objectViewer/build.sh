
#!/bin/bash

# Nvidia library was commected out in GNUMakefile

cd ./objViewStarter/

echo $1

if [ "$1" = 'clean' ]
then
    make clean
elif [ "$1" = 'dep' ]
then
    sudo apt-get install libglew-dev freeglut3-dev libglm-dev libxi-dev libxmu-dev
fi

make
var=$?

if [ $var == 0 ]
then
    ./bin/debug/objectViewer
fi

cd ../
exit 0
